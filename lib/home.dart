import 'package:flutter/material.dart';
import 'package:midterm_mobile/page1.dart';
import 'package:url_launcher/url_launcher.dart';

import 'about.dart';
import 'grade.dart';
import 'guide.dart';
import 'load.dart';
import 'main.dart';
import 'profile.dart';
import 'table.dart';

void main() {
  runApp(const Home());
}

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'RegBUU',
      // theme: ThemeData(
      //   primarySwatch: Colors.yellow,
      //   textTheme: TextTheme(),
      // ),
      home: const MyHomePage(title: 'REG BUU'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        drawer: Drawer(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                buildHeader(context),
                buildMenuItems(context),
              ],
            ),
          ),
        ),
        appBar: PreferredSize(
          //wrap with PreferredSize
          // preferredSize: Size.fromHeight(90), //height of appbar
          preferredSize: Size.fromHeight(97.0),
          child: AppBar(
            flexibleSpace: Container(
              //title:
              padding: const EdgeInsets.all(25.5),
              child: Column(
                children: <Widget>[
                  Image.asset(
                    'image/BuuUniv.png',
                    width: 150,
                  ),
                ],
              ),
              //Custom Appbar
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
                gradient: LinearGradient(colors: [
                  Color.fromARGB(255, 255, 208, 79),
                  Color.fromARGB(255, 250, 234, 55),
                ], begin: Alignment.bottomCenter, end: Alignment.topCenter),
              ),
              //Add Image AppBar
              // decoration: BoxDecoration(
              //   image: DecorationImage(
              //       image: AssetImage('image/buuSign.jpg'), fit: BoxFit.fill),
              // ),
            ),
            centerTitle: true,
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.account_circle_rounded,
                      size: 30.0, color: Color.fromARGB(255, 138, 138, 138)),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  tooltip:
                      MaterialLocalizations.of(context).openAppDrawerTooltip,
                );
              },
            ),
            backgroundColor: Colors.transparent,
            elevation: 0,
            //Action info
            actions: [
              PopupMenuButton<String>(
                icon: Icon(Icons.info_outline_rounded,
                    size: 25.0, color: Color.fromARGB(255, 255, 246, 246)),
                itemBuilder: (context) => [
                  PopupMenuItem(
                    child: Text('เกี่ยวกับทะเบียน'),
                    value: 'About',
                  ),
                  PopupMenuItem(
                    child: Text('แนะนำการลงทะเบียน'),
                    value: 'Guide',
                  ),
                  PopupMenuItem(
                    child: Text('Facebook'),
                    value: 'FB',
                  )
                ],
                onSelected: (choice) {
                  switch (choice) {
                    case 'About':
                      Navigator.pop(context);

                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const about(),
                        ),
                      );
                      break;
                    case 'Guide':
                      Navigator.pop(context);

                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const guide(),
                        ),
                      );
                      break;
                    case 'FB':
                      _launchFB();
                      break;
                  }
                },
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 12.0),
                child: Container(
                    alignment: Alignment.center,
                    height: 40,
                    width: 400,
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20)),
                          gradient: LinearGradient(
                              colors: [
                                Color.fromARGB(255, 255, 208, 79),
                                Color.fromARGB(255, 250, 234, 55),
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter),
                        ),
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(0.0),
                        width: 150.0,
                        height: 40.0,
                        child: Text(
                          'ประกาศเรื่อง',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    )),
              ),
              //Content 1
              Container(
                padding: EdgeInsets.only(top: 8.0),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 415.0,
                      height: 295.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image(
                          image: AssetImage("image/content/1.png"),
                          width: 390,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //Content 2
              Container(
                padding: EdgeInsets.only(top: 8.0),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 415.0,
                      height: 200.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image(
                          image: AssetImage("image/content/2.jpg"),
                          width: 390,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //Content 3
              Container(
                padding: EdgeInsets.only(top: 8.0),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 415.0,
                      height: 250.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image(
                          image: AssetImage("image/content/3.jpg"),
                          width: 390,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //Content 4
              Container(
                padding: EdgeInsets.only(top: 8.0),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 415.0,
                      height: 270.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image(
                          image: AssetImage("image/content/4.jpg"),
                          width: 390,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //Content 5
              Container(
                padding: EdgeInsets.only(top: 8.0),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 415.0,
                      height: 110.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image(
                          image: AssetImage("image/content/5.jpg"),
                          width: 390,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //Be Footer of BUU
              Container(
                padding: EdgeInsets.only(top: 8.0),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 415.0,
                      height: 50.0,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Text('Burapha University')),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )

        // SafeArea(
        //   child: ListView(
        //     children: <Widget>[],
        //   ),
        // ),
        );
  }

  _launchFB() async {
    const url = 'https://www.facebook.com/BuuReg';
    final uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget buildHeader(BuildContext context) => Material(
        color: Color.fromARGB(255, 255, 239, 61),
        child: InkWell(
          onTap: () {
            //next page
          },
          child: Container(
            padding: EdgeInsets.only(
                top: 24 + MediaQuery.of(context).padding.top, bottom: 24),
            child: Column(
              children: const [
                CircleAvatar(
                  radius: 52,
                  backgroundImage: NetworkImage(
                      'https://i.guim.co.uk/img/media/26392d05302e02f7bf4eb143bb84c8097d09144b/446_167_3683_2210/master/3683.jpg?width=1200&quality=85&auto=format&fit=max&s=a52bbe202f57ac0f5ff7f47166906403'),
                ),
                SizedBox(height: 12),
                Text('วิภาวดี ดียามา',
                    style: TextStyle(fontSize: 28, color: Colors.black54)),
                Text('63160080@go.buu.ac.th',
                    style: TextStyle(
                        fontSize: 15,
                        color: Color.fromARGB(255, 138, 138, 138))),
              ],
            ),
          ),
        ),
      );
  Widget buildMenuItems(BuildContext context) => Wrap(
        runSpacing: 5,
        children: [
          ListTile(
            // leading: const Icon(Icons.home_outlined),
            title: const Text('ลงทะเบียน'),
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('reg.buu.ac.th'),
                      content: Text(
                          'นิสิตทำการลงทะเบียนแล้วโปรดตรวจสอบรายวิชาที่เมนูผลการลงทะเบียน'),
                      actions: <Widget>[
                        TextButton(
                            onPressed: () => Navigator.of(context).pop(),
                            child: Text('OK')),
                      ],
                    );
                  });
            },
          ),
          ListTile(
            title: const Text('ผลการลงทะเบียน'),
            onTap: () {
              Navigator.pop(context);

              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const page1(),
                ),
              );
            },
          ),
          ListTile(
            title: const Text('ผลอนุมัติเพิ่ม-ลด'),
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('reg.buu.ac.th'),
                      content: Text(
                          'นิสิตจะสามารถเพิ่ม-ลดรายวิชาได้อีกครั้ง เมื่อผลการอนุมัติครบทุกรายการ และยังอยู่ในช่วงเพิ่ม-ลด'),
                      actions: <Widget>[
                        TextButton(
                            onPressed: () => Navigator.of(context).pop(),
                            child: Text('OK')),
                      ],
                    );
                  });
            },
          ),
          ListTile(
            title: const Text('ตารางเรียน/สอบ'),
            onTap: () {
              Navigator.pop(context);

              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const table(),
                ),
              );
            },
          ),
          ListTile(
            title: const Text('ประวัตินิสิต'),
            onTap: () {
              Navigator.pop(context);

              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const profile(),
                ),
              );
            },
          ),
          ListTile(
            title: const Text('ภาระค่าใช้จ่ายทุน'),
            onTap: () {
              Navigator.pop(context);

              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const load(),
                ),
              );
            },
          ),
          ListTile(
            title: const Text('ผลการศึกษา'),
            onTap: () {
              Navigator.pop(context);

              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const grade(),
                ),
              );
            },
          ),
          ListTile(
            title: const Text('ตรวจสอบจบ'),
            onTap: () {},
          ),
          const Divider(
            color: Colors.black54,
          ),
          ListTile(
            title: const Text('ยื่นคำร้อง'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('เสนอความคิดเห็น'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('รายชื่อนิสิต'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('ประวัติการเข้าใช้ระบบ'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('ตรวจหนี้สิน(นิสิตที่ยังไม่จบ)',
                style: TextStyle(color: Color.fromARGB(255, 222, 95, 86))),
            onTap: () {},
          ),
          ListTile(
            title: const Text('ออกจากระบบ'),
            onTap: () {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => const MyApp()),
                (Route<dynamic> route) => false,
              );
            },
          ),
        ],
      );
}
