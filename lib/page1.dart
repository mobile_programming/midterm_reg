import 'package:flutter/material.dart';

import 'Home.dart';

void main() {
  runApp(const page1());
}

class page1 extends StatelessWidget {
  const page1({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'RegBUU',
      // theme: ThemeData(
      //   primarySwatch: Colors.yellow,
      //   textTheme: TextTheme(),
      // ),
      home: Scaffold(
        body: Mypage1(
          title: 'Page1',
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.transparent,
            elevation: 0,
            child: Container(
              padding: EdgeInsets.all(20),
              child: Text(
                "Burapha University",
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ),
            )),
      ),
    );
  }
}

class Mypage1 extends StatefulWidget {
  const Mypage1({super.key, required this.title});

  final String title;

  @override
  State<Mypage1> createState() => _MypageState();
}

class _MypageState extends State<Mypage1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        //wrap with PreferredSize
        // preferredSize: Size.fromHeight(90), //height of appbar
        preferredSize: Size.fromHeight(97.0),
        child: AppBar(
          flexibleSpace: Container(
            //title:
            padding: const EdgeInsets.all(25.5),
            child: Column(
              children: <Widget>[
                Image.asset(
                  'image/BuuUniv.png',
                  width: 150,
                ),
              ],
            ),
            //Custom Appbar
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20)),
              gradient: LinearGradient(colors: [
                Color.fromARGB(255, 255, 208, 79),
                Color.fromARGB(255, 250, 234, 55),
              ], begin: Alignment.bottomCenter, end: Alignment.topCenter),
            ),
          ),
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_rounded,
                    size: 30.0, color: Color.fromARGB(255, 138, 138, 138)),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const Home(),
                    ),
                  );
                },
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              );
            },
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 150.0,
                      height: 40.0,
                      child: Text(
                        'ผลการลงทะเบียน',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              child: Text('ปีการศึกษา 2565 ภาคเรียนที่ 2'),
            ),
            //ผลการลงทะเบียน
            Container(
              padding: EdgeInsets.only(top: 4.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 235.0,
                    child: ClipRRect(
                      // borderRadius: BorderRadius.circular(20),
                      child: Image(
                        image: AssetImage("image/content/page1/1.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //คะแนน
            Padding(
              padding: EdgeInsets.only(top: 2.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 250,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 100.0,
                      height: 40.0,
                      child: Text(
                        'คะแนน',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            //ผลคะแนน
            Container(
              padding: EdgeInsets.only(top: 0.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 160.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/content/page1/2.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
