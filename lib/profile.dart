import 'package:flutter/material.dart';

import 'Home.dart';

void main() {
  runApp(const profile());
}

class profile extends StatelessWidget {
  const profile({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Table',
      home: Scaffold(
        body: MyProfile(
          title: 'Table',
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.transparent,
            elevation: 0,
            child: Container(
              padding: EdgeInsets.all(20),
              child: Text(
                "Burapha University",
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ),
            )),
      ),
    );
  }
}

class MyProfile extends StatefulWidget {
  const MyProfile({super.key, required this.title});

  final String title;

  @override
  State<MyProfile> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        //wrap with PreferredSize
        // preferredSize: Size.fromHeight(90), //height of appbar
        preferredSize: Size.fromHeight(97.0),
        child: AppBar(
          flexibleSpace: Container(
            //title:
            padding: const EdgeInsets.all(25.5),
            child: Column(
              children: <Widget>[
                Image.asset(
                  'image/BuuUniv.png',
                  width: 150,
                ),
              ],
            ),
            //Custom Appbar
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20)),
              gradient: LinearGradient(colors: [
                Color.fromARGB(255, 255, 208, 79),
                Color.fromARGB(255, 250, 234, 55),
              ], begin: Alignment.bottomCenter, end: Alignment.topCenter),
            ),
          ),
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_rounded,
                    size: 30.0, color: Color.fromARGB(255, 138, 138, 138)),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const Home(),
                    ),
                  );
                },
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              );
            },
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            //profile 1
            Padding(
              padding: EdgeInsets.only(top: 0.0),
              child: buildHeader(context),
            ),
            Container(
              padding: EdgeInsets.only(top: 0.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 400.0,
                    height: 1130.0,
                    child: ClipRRect(
                      child: Column(
                        children: [
                          Image(
                            image: AssetImage("image/profile/1.png"),
                            width: 390,
                          ),
                          Image(
                            image: AssetImage("image/profile/2.png"),
                            width: 390,
                          ),
                          Image(
                            image: AssetImage("image/profile/3.png"),
                            width: 390,
                          ),
                          Image(
                            image: AssetImage("image/profile/4.png"),
                            width: 390,
                          ),
                          Image(
                            image: AssetImage("image/profile/5.png"),
                            width: 290,
                            alignment: Alignment.bottomLeft,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHeader(BuildContext context) => Material(
        color: Colors.grey[200],
        child: InkWell(
          onTap: () {
            //next page
          },
          child: Container(
            padding: EdgeInsets.only(
                top: 24 + MediaQuery.of(context).padding.top, bottom: 24),
            child: Column(
              children: const [
                CircleAvatar(
                  radius: 52,
                  backgroundImage: NetworkImage(
                      'https://i.guim.co.uk/img/media/26392d05302e02f7bf4eb143bb84c8097d09144b/446_167_3683_2210/master/3683.jpg?width=1200&quality=85&auto=format&fit=max&s=a52bbe202f57ac0f5ff7f47166906403'),
                ),
                SizedBox(height: 12),
                Text('วิภาวดี ดียามา',
                    style: TextStyle(fontSize: 28, color: Colors.black54)),
              ],
            ),
          ),
        ),
      );
}
