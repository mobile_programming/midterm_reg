import 'package:flutter/material.dart';

import 'Home.dart';

void main() {
  runApp(const guide());
}

class guide extends StatelessWidget {
  const guide({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Guide',
      home: Scaffold(
        body: MyGuide(
          title: 'Guide',
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.transparent,
            elevation: 0,
            child: Container(
              padding: EdgeInsets.all(20),
              child: Text(
                "Burapha University",
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ),
            )),
      ),
    );
  }
}

class MyGuide extends StatefulWidget {
  const MyGuide({super.key, required this.title});

  final String title;

  @override
  State<MyGuide> createState() => _MyGuideState();
}

class _MyGuideState extends State<MyGuide> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        //wrap with PreferredSize
        // preferredSize: Size.fromHeight(90), //height of appbar
        preferredSize: Size.fromHeight(97.0),
        child: AppBar(
          flexibleSpace: Container(
            //title:
            padding: const EdgeInsets.all(25.5),
            child: Column(
              children: <Widget>[
                Image.asset(
                  'image/BuuUniv.png',
                  width: 150,
                ),
              ],
            ),
            //Custom Appbar
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20)),
              gradient: LinearGradient(colors: [
                Color.fromARGB(255, 255, 208, 79),
                Color.fromARGB(255, 250, 234, 55),
              ], begin: Alignment.bottomCenter, end: Alignment.topCenter),
            ),
          ),
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_rounded,
                    size: 30.0, color: Color.fromARGB(255, 138, 138, 138)),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const Home(),
                    ),
                  );
                },
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              );
            },
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 170.0,
                      height: 40.0,
                      child: Text(
                        'แนะนำการลงทะเบียน',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 200.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/guide/1.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 240.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/guide/2.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 280.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/guide/3.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 0.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 200.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/guide/4.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //ยืนยันการลงทะเบียน
            Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 170.0,
                      height: 40.0,
                      child: Text(
                        'ยืนยันการลงทะเบียน',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 0.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 200.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/guide/5.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
