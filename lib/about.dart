import 'package:flutter/material.dart';

import 'Home.dart';

void main() {
  runApp(const about());
}

class about extends StatelessWidget {
  const about({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'About',
      home: Scaffold(
        body: Myabout(
          title: 'About',
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.transparent,
            elevation: 0,
            child: Container(
              padding: EdgeInsets.all(20),
              child: Text(
                "Burapha University",
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ),
            )),
      ),
    );
  }
}

class Myabout extends StatefulWidget {
  const Myabout({super.key, required this.title});

  final String title;

  @override
  State<Myabout> createState() => _MyaboutState();
}

class _MyaboutState extends State<Myabout> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        //wrap with PreferredSize
        // preferredSize: Size.fromHeight(90), //height of appbar
        preferredSize: Size.fromHeight(97.0),
        child: AppBar(
          flexibleSpace: Container(
            //title:
            padding: const EdgeInsets.all(25.5),
            child: Column(
              children: <Widget>[
                Image.asset(
                  'image/BuuUniv.png',
                  width: 150,
                ),
              ],
            ),
            //Custom Appbar
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20)),
              gradient: LinearGradient(colors: [
                Color.fromARGB(255, 255, 208, 79),
                Color.fromARGB(255, 250, 234, 55),
              ], begin: Alignment.bottomCenter, end: Alignment.topCenter),
            ),
          ),
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_rounded,
                    size: 30.0, color: Color.fromARGB(255, 138, 138, 138)),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const Home(),
                    ),
                  );
                },
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              );
            },
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 150.0,
                      height: 40.0,
                      child: Text(
                        'ความเป็นมา',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            //ผลการลงทะเบียน
            Container(
              padding: EdgeInsets.only(top: 4.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 200.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/about/1.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //โครงสร้าง1
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 150.0,
                      height: 40.0,
                      child: Text(
                        'โครงสร้าง 1',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 240.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/about/2.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //โครงสร้าง2
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 150.0,
                      height: 40.0,
                      child: Text(
                        'โครงสร้าง 2',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 240.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/about/3.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //บริหารทั่วไป
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 150.0,
                      height: 40.0,
                      child: Text(
                        'บริหารทั่วไป',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 240.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/about/4.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //รับเข้า
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 150.0,
                      height: 40.0,
                      child: Text(
                        'รับเข้า',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 240.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/about/5.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //ทะเบียน1
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 150.0,
                      height: 40.0,
                      child: Text(
                        'ทะเบียน 1',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 240.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/about/6.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //ทะเบียน2
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 150.0,
                      height: 40.0,
                      child: Text(
                        'ทะเบียน 2',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 240.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/about/7.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //ทะเบียน3
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 150.0,
                      height: 40.0,
                      child: Text(
                        'ทะเบียน 3',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 240.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/about/8.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //ระบบ
            Padding(
              padding: EdgeInsets.only(top: 12.0),
              child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 400,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 255, 208, 79),
                              Color.fromARGB(255, 250, 234, 55),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter),
                      ),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(0.0),
                      width: 150.0,
                      height: 40.0,
                      child: Text(
                        'ระบบ',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 240.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/about/9.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
