import 'package:flutter/material.dart';

import 'Home.dart';

void main() {
  runApp(const grade());
}

class grade extends StatelessWidget {
  const grade({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Table',
      home: Scaffold(
        body: MyGrade(
          title: 'Table',
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.transparent,
            elevation: 0,
            child: Container(
              padding: EdgeInsets.all(20),
              child: Text(
                "Burapha University",
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ),
            )),
      ),
    );
  }
}

class MyGrade extends StatefulWidget {
  const MyGrade({super.key, required this.title});

  final String title;

  @override
  State<MyGrade> createState() => _MyGradeState();
}

class _MyGradeState extends State<MyGrade> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        //wrap with PreferredSize
        // preferredSize: Size.fromHeight(90), //height of appbar
        preferredSize: Size.fromHeight(97.0),
        child: AppBar(
          flexibleSpace: Container(
            //title:
            padding: const EdgeInsets.all(25.5),
            child: Column(
              children: <Widget>[
                Image.asset(
                  'image/BuuUniv.png',
                  width: 150,
                ),
              ],
            ),
            //Custom Appbar
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20)),
              gradient: LinearGradient(colors: [
                Color.fromARGB(255, 255, 208, 79),
                Color.fromARGB(255, 250, 234, 55),
              ], begin: Alignment.bottomCenter, end: Alignment.topCenter),
            ),
          ),
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_rounded,
                    size: 30.0, color: Color.fromARGB(255, 138, 138, 138)),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const Home(),
                    ),
                  );
                },
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              );
            },
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 0.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 180.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/grade/1.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 0.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 180.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/grade/2.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 0.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 180.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/grade/3.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 0.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 180.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/grade/4.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 0.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 180.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/grade/5.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 0.0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(0.0),
                    width: 415.0,
                    height: 180.0,
                    child: ClipRRect(
                      child: Image(
                        image: AssetImage("image/grade/6.png"),
                        width: 390,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
